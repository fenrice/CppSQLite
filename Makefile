SHELL = zsh
CXX = clang++-10
CXXFLAGS = -O0 -g -Wextra -Wall -std=c++17 $(DEFS)
SOURCES = main.cpp CppSqlite3.cpp
INC = CppSqlite3.h
OBJECTS = sqlite_example_1.o CppSQLite3.o
LIB = -lsqlite3
DEFS = -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
default : all

#main.o : main.cpp
#	$(CXX) -c main.cpp $(CXXFLAGS)

#CppSQLite3.o: CppSQLite3.cpp CppSQLite3.h
#	$(CXX) -c CppSQLite3.cpp $(CXXFLAGS)

#all : main.o CppSQLite3.o
#	$(CXX) -o test $(OBJECTS) $(LIB) $(CXXFLAGS) $(DEFS)

all: CppSQLite3.o sqlite_example_1.o
	$(CXX) -o test $(OBJECTS) $(LIB) $(CXXFLAGS) $(DEFS)

CppSQLite3.o : CppSQLite3.h

main.o : CppSQLite3.h

sqlite_example_1.o : CppSQLite3.h

.PHONY: clean print make_junk


FILES := $(wildcard *.cpp)

cfiles:
	$(info INFO: $(FILES:.cpp=.o))
	$(info $$(FILES): $(FILES) $(FILES:.cpp=.o))
	touch print

print: make_junk
	$(info PREREQUISITE TARGETS: $^)
	$(info TARGET: $@)
	$(info FIRST PREREQ: $<)

make_junk:
	END=3; \
	for (( i=0; i < $${END}; i++)); do \
		echo junk$${i}.o ; \
	done

clean:
	-rm $(OBJECTS) test

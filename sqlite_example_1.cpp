#include <iostream>
#include <ctime>
#include "CppSQLite3.h"
using namespace std;

const char* gszFile = "/tmp/test.db";

int main(void)
{

  try {
    int i, fld;
    time_t tmStart, tmEnd;
    CppSQLite3DB db;

    //cout <<  "SQLite Version" << db.SqLite3Version() << endl
    std::remove(gszFile);
    db.open(gszFile);

    cout << endl << "Creating emp table" << endl;
    db.execDML("create table emp(id integer primary key autoincrement, empno int, empname char(20));");

    cout << endl << "DML Tests" << endl;
    int nRows = db.execDML("insert into emp (empno, empname) values (7, 'David Beckham');");
    nRows += db.execDML("insert into emp (empno, empname) values (8, 'Millard Fillmore');");
    nRows += db.execDML("insert into emp (empno, empname) values (9, 'Willard Fillmore');");
    nRows += db.execDML("insert into emp (empno, empname) values (10, 'Don Owens');");
    cout << "nRows created: " << nRows << endl;

    int nRowsToCreate(500000);
    tmStart = time(0);
    db.execDML("begin transaction;");
    for (auto i=0; i < nRowsToCreate; i++) {
      char buf[128];
      sprintf(buf, "insert into emp (empno, empname) values (%d, 'Empname%06d');", i, i);
      db.execDML(buf);
    }
    db.execDML("commit transaction;");
    tmEnd = time(0);
    cout << "Finished transaction in " << tmEnd-tmStart << " seconds." << endl;
    db.close();

  }
  catch(CppSQLite3Exception &e) {
    cerr << e.errorCode() << ":" << e.errorMessage() << endl;
  }
  return 0;
}
